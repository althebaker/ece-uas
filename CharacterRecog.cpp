// Implementation of the class CharacterRecog
// Defining the member functions

#include <iostream>
#include <opencv/cv.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using std::cout;
using namespace cv;
using namespace std;


#include "CharacterRecog.h"

// Constructor

RNG rng(12345);
CharacterRecog::CharacterRecog()
{
	//capture_image=0;
	//hsv_image=0;
	//thresh_image=0;
	//clone_image=0;
	//draw_contours=0;


	Width=0;
	Height=0;
	edge_thresh=50;
	max_thresh=255; 
	
}

void CharacterRecog::getImage(char* image_filepath)
{
	capture_image= imread(image_filepath,CV_LOAD_IMAGE_UNCHANGED);
	Width=capture_image.size().width;
	Height=capture_image.size().height;

}

Mat CharacterRecog::getThresholdImage(Mat image1_hsv)
{
	//Mat image_thresh;
	inRange(image1_hsv,Scalar(0,100,150),Scalar(180,256,190),thresh_image);

	return(thresh_image);

}

void CharacterRecog::morph_operation(Mat image_thresh)
{
	int erosion_size=2;

	Mat elem1=getStructuringElement(MORPH_RECT,Size(erosion_size,erosion_size),Point(-1,-1));
	Mat elem2=getStructuringElement(MORPH_RECT,Size(2*erosion_size,2*erosion_size),Point(-1,-1));


	dilate(image_thresh,image_thresh,elem1);
	erode(image_thresh,image_thresh,elem2);
	erode(image_thresh,image_thresh,elem2);
	dilate(image_thresh,image_thresh,elem1);



}

void CharacterRecog::Draw_Contours(int edge_thresh,Mat thresh_image)
{

	int k;
	Mat canny_output;
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;

	//Detect edge using Canny edge detector.
	Canny(thresh_image,canny_output,edge_thresh,edge_thresh*2,3);

	//Find contours.
	findContours(canny_output,contours,hierarchy,CV_RETR_TREE,CV_CHAIN_APPROX_SIMPLE,Point(0,0));

	//Approximate contours to polygons and get bounding rectangles and circles.
	vector<vector<Point> >contours_poly(contours.size());
	vector<Rect>bounding_rect(contours.size());
	vector<Point2f>center(contours.size());
	vector<float>radius(contours.size());

	for(k=0; k<contours.size();k++)
	{

		approxPolyDP(Mat(contours[k]),contours_poly[k],3,true);
		bounding_rect[k]=boundingRect(Mat(contours_poly[k]));
		minEnclosingCircle((Mat)contours_poly[k],center[k],radius[k]);

	}

	//Draw the polygonal contours, bounding rectangles and circles.
	draw_contours= Mat::zeros(canny_output.size(),CV_8UC3);

	for(k=0; k<contours.size(); k++)
	{

		Scalar color= Scalar(rng.uniform(0,255),rng.uniform(0,255),rng.uniform(0,255));
		drawContours(draw_contours,contours_poly,k,color,1,8,hierarchy,0,Point());
		rectangle(draw_contours,bounding_rect[k].tl(),bounding_rect[k].br(),color,2,8,0);
		circle(draw_contours,center[k],(int)radius[k],color,2,8,0);

	}

		
}

void CharacterRecog::ObjectDetection()
{

	cvtColor(capture_image,hsv_image,CV_BGR2HSV);
	thresh_image=getThresholdImage(hsv_image);
	Draw_Contours(edge_thresh,thresh_image);


}

void CharacterRecog::Write_Image(const char* image_name, const char* out_dir)
{
  char out_path [128];
  int out_dir_len = 0;
  int image_name_len = 0;
  Mat out_image = draw_contours.clone();

  //cvtColor(out_image, draw_contours, HSV2BGR);

  if((out_dir_len = strlen(out_dir)) > 0)
  {
    strcpy(out_path, out_dir);
  }
  
  if((image_name_len = strlen(image_name)) == 0)
  {
    return;
  }
  strcpy(&out_path[out_dir_len], image_name);

  strcpy(&out_path[out_dir_len + image_name_len], ".png");

  imwrite(out_path, out_image);
}

void CharacterRecog::Display_Image()
{

	namedWindow("Captured Image",CV_WINDOW_AUTOSIZE);
	namedWindow("HSV Image",CV_WINDOW_AUTOSIZE);
	namedWindow("Threshold Image",CV_WINDOW_AUTOSIZE);
	namedWindow("Contours",CV_WINDOW_AUTOSIZE);

	imshow("Captured Image",capture_image);
	imshow("HSV Image",hsv_image);
	imshow("Threshold Image",thresh_image);
	imshow("Contours",draw_contours);

	cvWaitKey(0);

	destroyWindow("Captured Image");
	destroyWindow("HSV Image");
	destroyWindow("Threshold Image");
	destroyWindow("Contours");


}

