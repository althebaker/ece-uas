// Header file for Character Recognition class.
//Member functions are defined in CharacterRecog.cpp

#ifndef CHARACTERRECOG_H
#define CHARACTERRECOG_H

#include <iostream>
#include <opencv/cv.h>
#include <opencv2/highgui/highgui.hpp>

class CharacterRecog
{

private:

  cv::Mat capture_image;
  cv::Mat hsv_image;
  cv::Mat thresh_image;
  cv::Mat clone_image;
  cv::Mat draw_contours;

  int Width;
  int Height;
  int edge_thresh;
  int max_thresh;
	
public:

  CharacterRecog();
  void getImage(char*);

  cv::Mat getThresholdImage(cv::Mat);
  void morph_operation(cv::Mat);

  void Draw_Contours(int,cv::Mat);

  void ObjectDetection();
  void Display_Image();
  void Write_Image(const char*, const char*);
};

#endif
