tb_CharacterRecog: tb_CharacterRecog.cpp CharacterRecog.o | out
	g++ -g -pg `pkg-config --cflags opencv` tb_CharacterRecog.cpp CharacterRecog.o -o tb_CharacterRecog `pkg-config --libs opencv`

CharacterRecog.o: CharacterRecog.cpp CharacterRecog.h
	g++ -g -pg -c -o CharacterRecog.o $< 

out:
	-mkdir out

clean:
	-rm -rf tb_CharacterRecog
	-rm -rf CharacterRecog.o
	-rm out/*
