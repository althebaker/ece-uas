#include "CharacterRecog.h"

const char* tb_image_names [] = {
  "A",
  "E",
  "F",
  "J",
  "L",
  "O",
  "S",
  "T",
  "U",
  "Y",
  "airstrip1",
  "blur1",
  "cutoff1",
  "cutoff2",
  "multi1",
  "orientation1",
  "orientation2",
  "orientation3",
  "orientation4",
  "orientation5",
  "orientation6",
  "orientation7",
  "orientation8",
  "simple",
  NULL
};

const char* tb_image_filepaths [] = {
  "test/png/A.png",
  "test/png/E.png",
  "test/png/F.png",
  "test/png/J.png",
  "test/png/L.png",
  "test/png/O.png",
  "test/png/S.png",
  "test/png/T.png",
  "test/png/U.png",
  "test/png/Y.png",
  "test/png/airstrip1.png",
  "test/png/blur1.png",
  "test/png/cutoff1.png",
  "test/png/cutoff2.png",
  "test/png/multi1.png",
  "test/png/orientation1.png",
  "test/png/orientation2.png",
  "test/png/orientation3.png",
  "test/png/orientation4.png",
  "test/png/orientation5.png",
  "test/png/orientation6.png",
  "test/png/orientation7.png",
  "test/png/orientation8.png",
  "test/png/simple.png",
  NULL
};

void Process_Image(const char*, char*, const char*);

int main(int argc, char** argv)
{
  int i = 0;
  const char* tb_image_filepath = tb_image_filepaths[i];
  int tb_image_filepath_prefix_len = 0;
  char tb_image_filepath_full [128];

  if (argc > 1)
  {
    if ((tb_image_filepath_prefix_len = strlen(argv[1])) > 64)
    {
      printf("ERROR: Filepath prefix too long.\n");
      return -1;
    }
    strcpy(tb_image_filepath_full, argv[1]);
  }

  while (tb_image_filepath != NULL)
  {
    strcpy(&tb_image_filepath_full[tb_image_filepath_prefix_len], tb_image_filepath);
    printf("INFO: Processing %s...\n", tb_image_filepath);
    Process_Image(tb_image_names[i], tb_image_filepath_full, "out/");
    tb_image_filepath = tb_image_filepaths[++i];
  }

  return 0;
}

void Process_Image(const char* image_name, char* image_filepath, const char* out_dir)
{
  CharacterRecog recog;
  recog.getImage(image_filepath);
  recog.ObjectDetection();
  recog.Write_Image(image_name, out_dir);

  return;
}
